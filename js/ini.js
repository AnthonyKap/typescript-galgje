$(document).ready(function () {
    var letters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    var arrayLetters = Array.from(letters);
    new Vue({
        el: '#keypad',
        data: {
            object: arrayLetters
        }
    });
    var game = new Galgje();
    game.play();
    $('button.new-game').click(function () {
        location.reload();
    });
});
//# sourceMappingURL=ini.js.map