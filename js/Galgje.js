var Galgje = (function () {
    function Galgje() {
        this.getWords();
    }
    Galgje.prototype.getWords = function () {
        if (localStorage.getItem("words") === null) {
            $.getJSON("js/words.json", function (data) {
                var words = [];
                $.each(data, function (key, val) {
                    words.push(val);
                });
                if (typeof (Storage) !== "undefined") {
                    localStorage.setItem("words", JSON.stringify(words));
                }
            });
        }
    };
    Galgje.prototype.getRandomWord = function () {
        var words = JSON.parse(localStorage.getItem("words"));
        var index = Math.floor(Math.random() * words.length);
        var word = words.splice(index, 1);
        if (words.length >= 1) {
            localStorage.setItem("words", JSON.stringify(words));
        }
        else {
            localStorage.clear();
            this.getWords();
        }
        return word.toString();
    };
    Galgje.prototype.play = function () {
        var attemptsLeft = 10;
        var guessedCount = 0;
        var guessed = false;
        var word = this.getRandomWord().split('');
        var placeholder = [];
        for (var i = 0; i < word.length; i++) {
            placeholder[i] = '_';
        }
        var underscores = '';
        placeholder.forEach(function getPlaceholders(value) {
            underscores += value + ' ';
        });
        new Vue({
            el: '#letters',
            data: {
                letters: underscores,
            }
        });
        var guess = '';
        $('button.letter-button').click(function (event) {
            guess = ($(this).attr('id'));
            $(this).prop('disabled', true);
            word.forEach(function evaluateGuess(value, index) {
                if (guess === value.toUpperCase()) {
                    guessedCount++;
                    placeholder[index] = guess;
                    guessed = true;
                }
                if ((guessedCount === word.length) && (attemptsLeft >= 1)) {
                    $('.letter-button').prop('disabled', true);
                    $('#letters').css('color', 'green');
                    $('#game-status').text('Gefeliciteerd, je hebt gewonnen!').css('color', 'green');
                }
            });
            if (guessed === true) {
                guessed = false;
            }
            else {
                attemptsLeft--;
                switch (attemptsLeft) {
                    case 9:
                        $('use#Rectangle-1').css({ 'stroke': '#000', 'fill': '#000' });
                        break;
                    case 8:
                        $('use#Rectangle-2').css({ 'stroke': '#000', 'fill': '#000' });
                        break;
                    case 7:
                        $('use#Rectangle-3').css({ 'stroke': '#000', 'fill': '#000' });
                        break;
                    case 6:
                        $('use#Rectangle-4').css({ 'stroke': '#000', 'fill': '#000' });
                        break;
                    case 5:
                        $('ellipse#Oval-1').css({ 'stroke': '#000', 'fill': '#000' });
                        $('g#Group path').css({ 'stroke': '#000' });
                        break;
                    case 4:
                        $('use#Rectangle-5').css({ 'stroke': '#000', 'fill': '#000' });
                        break;
                    case 3:
                        $('use#Rectangle-6').css({ 'stroke': '#000', 'fill': '#000' });
                        break;
                    case 2:
                        $('use#Rectangle-9').css({ 'stroke': '#000', 'fill': '#000' });
                        break;
                    case 1:
                        $('use#Rectangle-7').css({ 'stroke': '#000', 'fill': '#000' });
                        break;
                    case 0:
                        $('use#Rectangle-8').css({ 'stroke': '#000', 'fill': '#000' });
                        $('g#Group path').css({ 'stroke': '#FFF' });
                        break;
                }
            }
            if (attemptsLeft === 0) {
                $('.letter-button').prop('disabled', true);
                placeholder.forEach(function showResult(value, index) {
                    if (value === '_') {
                        placeholder[index] = word[index].toUpperCase();
                    }
                });
                $('#letters').css('color', 'red');
                $('#game-status').text('Helaas, je hebt het niet geraden!').css('color', 'red');
            }
            underscores = '';
            placeholder.forEach(function getPlaceholders(value) {
                underscores += value + ' ';
            });
            $('#letters').text(underscores);
        });
    };
    return Galgje;
}());
