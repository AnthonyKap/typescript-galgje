/// <reference path="jquery.d.ts" />
/// <reference path="vue.d.ts" />

class Galgje {

    constructor() {
        this.getWords();
    }

    /**
     * Get all words available for playing.
     */
    private getWords() {
        if(localStorage.getItem("words") === null) {
            $.getJSON("js/words.json", function( data ) {
                let words: Array<string> = [];
                $.each(data, function (key, val) {
                    words.push(val);
                });
                if(typeof(Storage) !== "undefined") {
                    localStorage.setItem("words", JSON.stringify(words));
                }
            });
        }
    }

    /**
     * Get a random word.
     * @return {string}
     *   Returns a word string.
     */
    private getRandomWord() {
        // Create an array with the stored values.
        let words: Array<string> = JSON.parse(localStorage.getItem("words"));
        // Get random index.
        let index: number = Math.floor(Math.random() * words.length);
        // Remove the word for given index from the array, and save the value.
        let word = words.splice(index, 1);

        if(words.length >= 1) {
            // Removes items (played words) from local storage.
            localStorage.setItem("words", JSON.stringify(words));
        }
        else {
            // If last item, load words.
            localStorage.clear();
            this.getWords();
        }
        return word.toString();
    }

    /**
     * Play a game.
     * */
    play() {
        let attemptsLeft: number = 10;
        let guessedCount: number = 0;
        let guessed: boolean = false;
        let word = this.getRandomWord().split('');

        // Create letter placeholder. Each letter will be represented by an underscore.
        let placeholder: Array<string> = [];
        for (let i = 0; i < word.length; i++) {
            placeholder[i] = '_';
        }
        // Create a string of underscores followed by an empty space to represent the placeholder.
        let underscores = '';
        placeholder.forEach(
        function getPlaceholders(value){
            underscores += value + ' ';
        });
        // Output the placeholder for the word in play.
        new Vue({
            el: '#letters',
            data: {
                letters: underscores,
            }
        });

        // Catch and save the player's guess.
        let guess: string = '';
        $('button.letter-button').click(function(event){
            guess = ($(this).attr('id'));
            // Disable the button for the player's guess.
            $(this).prop('disabled', true);

            // Evaluate the guess.
            word.forEach(function evaluateGuess(value, index){
                if(guess === value.toUpperCase()) {
                    guessedCount++;
                    placeholder[index] = guess;
                    guessed = true;
                }

                // Check if game is done and you win.
                if ((guessedCount === word.length) && (attemptsLeft >= 1)) {
                  // If the the game is done, disable all buttons
                  $('.letter-button').prop('disabled', true);
                  $('#letters').css('color', 'green');
                  $('#game-status').text('Gefeliciteerd, je hebt gewonnen!').css('color', 'green');
                }
            });
            if(guessed === true) {
                guessed = false;
            }
            else {
                attemptsLeft--;
                switch(attemptsLeft) {
                    case 9:
                        $('use#Rectangle-1').css({'stroke':'#000', 'fill':'#000'});
                        break;
                    case 8:
                        $('use#Rectangle-2').css({'stroke':'#000', 'fill':'#000'});
                        break;
                    case 7:
                        $('use#Rectangle-3').css({'stroke':'#000', 'fill':'#000'});
                        break;
                    case 6:
                        $('use#Rectangle-4').css({'stroke':'#000', 'fill':'#000'});
                        break;
                    case 5:
                        $('ellipse#Oval-1').css({'stroke':'#000', 'fill':'#000'});
                        $('g#Group path').css({'stroke':'#000'});
                        break;
                    case 4:
                        $('use#Rectangle-5').css({'stroke':'#000', 'fill':'#000'});
                        break;
                    case 3:
                        $('use#Rectangle-6').css({'stroke':'#000', 'fill':'#000'});
                        break;
                    case 2:
                        $('use#Rectangle-9').css({'stroke':'#000', 'fill':'#000'});
                        break;
                    case 1:
                        $('use#Rectangle-7').css({'stroke':'#000', 'fill':'#000'});
                        break;
                    case 0:
                        $('use#Rectangle-8').css({'stroke':'#000', 'fill':'#000'});
                        $('g#Group path').css({'stroke':'#FFF'});
                        break;
                }
            }

            // If you lose
            if (attemptsLeft === 0) {
                $('.letter-button').prop('disabled', true);
                // Replace value of remaining underscores for the actual letter value from the word in play.
                placeholder.forEach(function showResult(value, index){
                    if(value === '_') {
                        placeholder[index] = word[index].toUpperCase();
                    }
                });
                $('#letters').css('color', 'red');
                $('#game-status').text('Helaas, je hebt het niet geraden!').css('color', 'red');
            }

            // Reset the value of underscores.
            underscores = '';
            // Save the value of the word in play into underscores.
            placeholder.forEach(
            function getPlaceholders(value){
                underscores += value + ' ';
            });
            // Present the word that was in play and player failed to guess.
            $('#letters').text(underscores);
        });
    }
}
