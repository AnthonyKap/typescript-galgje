$(document).ready(function(){

  // Get the letter buttons ready
  let letters: string = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
  let arrayLetters = Array.from(letters);
  new Vue({
      el: '#keypad',
      data: {
      object: arrayLetters
      }
  });

  // Instantiate new game.
  let game = new Galgje();
  game.play();

  // Start a new game
  $('button.new-game').click(function(){
      location.reload();
  });

});

interface ArrayConstructor {
	from(arrayLike: any, mapFn?, thisArg?): Array<any>;
}
