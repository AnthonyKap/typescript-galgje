<!DOCTYPE html>
<html>
<head>
    <link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="css/sticky-footer.css" rel="stylesheet">
    <link href="css/app.css" rel="stylesheet">
    <link rel="shortcut icon" href="data:image/x-icon;," type="image/x-icon">
    <link href='https://fonts.googleapis.com/css?family=Ultra|Roboto:300' rel='stylesheet' type='text/css'>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.6.2/html5shiv.js"></script>
    <script src="https://code.jquery.com/jquery.min.js"></script>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/vue/1.0.16/vue.js"></script>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width">
    <title>Galgje</title>
</head>
<body>

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 header-title">
                <h1 id="title">Welkom bij galgje!</h1>
                <p class="lead">Kan jij het woord raden?</p>
            </div>

            <div class="col-md-12 header-title" class="alert alert-warning">
              <div class="alert alert-warning alert-danger" role="alert">
                <strong>Een klein probleempje: </strong>wanneer je deze game voor het eerst wilt spelen moet de pagina even herladen worden, dan pakt hij de game wel goed.
              </div>
            </div>

            <div class="col-md-offset-1 col-md-5">
                <svg width="100%" height="385px" viewBox="201 -1 281 385" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                    <!-- Generator: Sketch 3.7.2 (28276) - http://www.bohemiancoding.com/sketch -->
                    <desc>Created with Sketch.</desc>
                    <defs>
                        <!-- base -->
                        <rect id="path-1" x="201" y="346.051639" width="280.703532" height="37.9132043"></rect>
                        <mask id="mask-2" maskContentUnits="userSpaceOnUse" maskUnits="objectBoundingBox" x="0" y="0" width="280.703532" height="37.9132043" fill="white">
                            <use xlink:href="#path-1"></use>
                        </mask>
                        <rect id="path-3" x="201" y="-1" width="7.29100083" height="347.78074"></rect>
                        <mask id="mask-4" maskContentUnits="userSpaceOnUse" maskUnits="objectBoundingBox" x="0" y="0" width="7.29100083" height="347.78074" fill="white">
                            <use xlink:href="#path-3"></use>
                        </mask>
                        <rect id="path-5" x="201" y="-1" width="204.877123" height="7.29100083"></rect>
                        <mask id="mask-6" maskContentUnits="userSpaceOnUse" maskUnits="objectBoundingBox" x="0" y="0" width="204.877123" height="7.29100083" fill="white">
                            <use xlink:href="#path-5"></use>
                        </mask>
                        <!-- rope -->
                        <rect id="path-7" x="347.549117" y="-1" width="6.56190075" height="60.5153069"></rect>
                        <mask id="mask-8" maskContentUnits="userSpaceOnUse" maskUnits="objectBoundingBox" x="0" y="0" width="6.56190075" height="60.5153069" fill="white">
                            <use xlink:href="#path-7"></use>
                        </mask>
                        <!-- torso -->
                        <rect id="path-9" x="322.759714" y="105.448612" width="72.1809082" height="113.739613" rx="8"></rect>
                        <mask id="mask-10" maskContentUnits="userSpaceOnUse" maskUnits="objectBoundingBox" x="0" y="0" width="72.1809082" height="113.739613" fill="white">
                            <use xlink:href="#path-9"></use>
                        </mask>
                        <!-- left arm -->
                        <rect id="path-11" x="296.512111" y="109.823213" width="20.4148023" height="93.3248106" rx="8"></rect>
                        <mask id="mask-12" maskContentUnits="userSpaceOnUse" maskUnits="objectBoundingBox" x="0" y="0" width="20.4148023" height="93.3248106" fill="white">
                            <use xlink:href="#path-11"></use>
                        </mask>
                        <!-- left leg -->
                        <rect id="path-13" x="333.696215" y="222.833725" width="20.4148023" height="93.3248106" rx="8"></rect>
                        <mask id="mask-14" maskContentUnits="userSpaceOnUse" maskUnits="objectBoundingBox" x="0" y="0" width="20.4148023" height="93.3248106" fill="white">
                            <use xlink:href="#path-13"></use>
                        </mask>
                        <!-- right leg -->
                        <rect id="path-15" x="362.860218" y="222.833725" width="20.4148023" height="93.3248106" rx="8"></rect>
                        <mask id="mask-16" maskContentUnits="userSpaceOnUse" maskUnits="objectBoundingBox" x="0" y="0" width="20.4148023" height="93.3248106" fill="white">
                            <use xlink:href="#path-15"></use>
                        </mask>
                        <!-- right arm -->
                        <rect id="path-17" x="400.773423" y="109.823213" width="20.4148023" height="93.3248106" rx="8"></rect>
                        <mask id="mask-18" maskContentUnits="userSpaceOnUse" maskUnits="objectBoundingBox" x="0" y="0" width="20.4148023" height="93.3248106" fill="white">
                            <use xlink:href="#path-17"></use>
                        </mask>
                    </defs>
                    <use id="Rectangle-1" stroke="#ebebe0" mask="url(#mask-2)" stroke-width="2" fill="#ebebe0" fill-rule="evenodd" xlink:href="#path-1"></use>
                    <use id="Rectangle-2" stroke="#ebebe0" mask="url(#mask-4)" stroke-width="2" fill="#ebebe0" fill-rule="evenodd" xlink:href="#path-3"></use>
                    <use id="Rectangle-3" stroke="#ebebe0" mask="url(#mask-6)" stroke-width="2" fill="#ebebe0" fill-rule="evenodd" xlink:href="#path-5"></use>
                    <use id="Rectangle-4" stroke="#ebebe0" mask="url(#mask-8)" stroke-width="2" fill="#ebebe0" fill-rule="evenodd" xlink:href="#path-7"></use>
                    <use id="Rectangle-5" stroke="#ebebe0" mask="url(#mask-10)" stroke-width="2" fill="#ebebe0" fill-rule="evenodd" xlink:href="#path-9"></use>
                    <use id="Rectangle-6" stroke="#ebebe0" mask="url(#mask-12)" stroke-width="2" fill="#ebebe0" fill-rule="evenodd" xlink:href="#path-11"></use>
                    <use id="Rectangle-7" stroke="#ebebe0" mask="url(#mask-14)" stroke-width="2" fill="#ebebe0" fill-rule="evenodd" xlink:href="#path-13"></use>
                    <use id="Rectangle-8" stroke="#ebebe0" mask="url(#mask-16)" stroke-width="2" fill="#ebebe0" fill-rule="evenodd" xlink:href="#path-15"></use>
                    <use id="Rectangle-9" stroke="#ebebe0" mask="url(#mask-18)" stroke-width="2" fill="#ebebe0" fill-rule="evenodd" xlink:href="#path-17"></use>
                    <!-- head -->
                    <ellipse id="Oval-1" stroke="#ebebe0" stroke-width="1" fill="#ebebe0" fill-rule="evenodd" cx="342.445416" cy="77.0137089" rx="31.3513036" ry="31.3513036"></ellipse>
                    <!-- dead face -->
                    <g id="Group" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" transform="translate(320.000000, 61.513709)">
                        <path d="M0.169048887,15.5588254 L12.7203929,8.86021839" id="Path-1" stroke="#ebebe0" stroke-width="3"></path>
                        <path d="M5.27274947,5.94381806 L10.1884789,17.2904381" id="Path-2" stroke="#ebebe0" stroke-width="3"></path>
                        <path d="M21.3129513,9.72602474 L33.8642953,3.02741773" id="Path-1" stroke="#ebebe0" stroke-width="3"></path>
                        <path d="M26.4166519,0.111017401 L31.3323813,11.4576374" id="Path-2" stroke="#ebebe0" stroke-width="3"></path>
                        <path d="M34.4367528,34.1081568 C34.4367528,34.1081568 35.4535056,22.59635 41.4913657,18.3385195" id="Path-3" stroke="#ebebe0" stroke-width="3"></path>
                    </g>
                </svg>
            </div>
            <div class="col-md-5">
                <p class="lead" id="letters">{{ letters }}</p>
                <span id="keypad">
                    <p id="game-status">Raad het woord:</p>
                    <button class="btn btn-default letter-button" v-for="value in object" id="{{ value }}">{{ value }}</button>
                </span>
            </div>
            <div class="col-md-5">
                <button class="btn btn-primary btn-lg new-game">Nieuw spel</button>
            </div>
        </div>
    </div>

    <script src="js/ini.js"></script>
    <script src="js/Galgje.js"></script>

</body>
</html>
